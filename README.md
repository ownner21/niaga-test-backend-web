
Cara Menggunakan
===
lakukan ``compser install`` untuk mengaktifkan project laravel
lakukan ``npm install `` untuk aktifkan nodejs 

1. sesuaikan koneksi DB di file .env (bisa copas dari .env.example)
2. Ketika perintah pada terminal ``php artisan migrate --seed``
3. crud user dapat diakses mengaktfikan server laravel ```php artisan serve``` dan defaultnya dapat diakses pada url ```localhost:8000/``` atau perhatikan log yang ada pada terminal port berapa project ini jalan, dan sesuaikan port jika tidak sesuai.

4. untuk menjalankan server socket ``node server.js`` berjalan di port 1234


====================

akses admin 
```
localhost:8000/admin
```

```
'email' => 'admin@email.com',
'password' => bcrypt('rahasia'),
'name' => 'admin',
```