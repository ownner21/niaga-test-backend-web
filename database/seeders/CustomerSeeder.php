<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create('id_ID');
 
    	for($i = 1; $i <= 10; $i++){
            $data[] = [
                'email' => $faker->email(),
                'password' => bcrypt('rahasia'),
                'first_name' => $faker->firstName(),
                'last_name' => $faker->lastName(),
                'number_ktp' => $faker->creditCardNumber(),
                'number_phone' => $faker->e164PhoneNumber(),
                'number_npwp' => $faker->creditCardNumber(),
                'address' => $faker->address(),
            ];
        }
        DB::table('customers')->truncate();
        DB::table('customers')->insert($data);
    }
}
