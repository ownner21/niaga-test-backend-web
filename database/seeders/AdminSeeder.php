<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create('id_ID');
 
    	for($i = 1; $i <= 5; $i++){
            $data[] = [
                'email' => $faker->email(),
                'password' => bcrypt('rahasia'),
                'name' => $faker->firstName(),
            ];
        }
        
        $data[] = [
            'email' => 'admin@email.com',
            'password' => bcrypt('rahasia'),
            'name' => 'admin',
        ];

        DB::table('admins')->truncate();
        DB::table('admins')->insert($data);
 
        //
    }
}
