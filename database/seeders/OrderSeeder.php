<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Log;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create('id_ID');
 
        $itemNames = [
            'Maiann','Makanan','Sayuran','Perabot'
        ];
        
        
    	for($i = 1; $i <= 100; $i++){
            for ($k=0; $k < rand(1,5) ; $k++) { 
                $items[] = [ 
                    'name' => $itemNames[array_rand($itemNames, 1)], 
                    'weight' => rand(1000,4000),
                    "p" => rand(100,200),
                    "l" => rand(200,250),
                    "t" => rand(100,250)
                ];
            }

            $data[] = [
                'customer_id' => rand(1, 10),
                'number_job' => 'NG'.$faker->numberBetween(10000,99999).'-'.date('mdHis'),
                'billing_address'=> json_encode([
                    'name' => $faker->name(),
                    'phone' => $faker->e164PhoneNumber(),
                    'address' => $faker->address(),
                ]),
                'shipping_address' => json_encode([
                    'name' => $faker->name(),
                    'phone' => $faker->e164PhoneNumber(),
                    'address' => $faker->address(),
                ]),
                'items' => json_encode($items),
                'total_qty' => count($items),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];

            $items = [];
        }
        DB::table('orders')->truncate();
        DB::table('orders')->insert($data);
    }
}
