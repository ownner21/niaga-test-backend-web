<?php

use App\Http\Controllers\Customer\ChatController;
use App\Http\Controllers\Customer\HomeController;
use App\Http\Controllers\Customer\LoginController;
use App\Http\Controllers\Customer\OrderController;
use Illuminate\Support\Facades\Route;

Route::view('/register', 'customer.register')->name('customer.register');
Route::redirect('/login','/')->name('customer.login');
Route::post('/login/form', [LoginController::class, 'loginForm'])->name('customer.login.form');
Route::post('/register', [LoginController::class, 'register'])->name('customer.register.store');
Route::post('/logout', [LoginController::class, 'logout'])->name('customer.logout');

Route::middleware('auth:customer')->group(function(){

    Route::controller(HomeController::class)->group(function(){
        Route::get('/', 'index')->name('customer.home');
        Route::get('/profil/edit', 'profiledit')->name('customer.profil.edit');
        Route::put('/profil/update', 'profilUpdate')->name('customer.profil.update');
    });
    
    
    Route::controller(OrderController::class)->group(function(){
        Route::get('/order/create','create')->name('customer.order.create');
        Route::get('/orderId/{no_job}','show')->name('customer.order.show');
        Route::post('/order/store', 'store')->name('customer.order.store');
        Route::post('/order/data','data')->name('customer.order.data');
    });
    
    Route::controller(ChatController::class)->group(function(){
        Route::get('chat-check/{auth}/{id}','check')->name('customer.chat.chek');
        Route::get('chat-room','room')->name('customer.chat.room');
        Route::get('chat/{key}','chat')->name('customer.chat.now');
        Route::post('chat-save','save')->name('customer.chat.save');
    });
});