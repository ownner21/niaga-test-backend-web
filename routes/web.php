<?php

use App\Http\Controllers\Customer\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('customer.home');
})->name('home');

Route::view('/notification','welcome');
Route::get('/auth/github/redirect', [LoginController::class, 'redirectGithub'])->name('customer.login.github');
Route::get('/auth/github/callback',  [LoginController::class, 'authGithub']);
Route::get('/auth/google/redirect',  [LoginController::class, 'redirectGoogle'])->name('customer.login.google');
Route::get('/auth/google/callback',  [LoginController::class, 'authGoogle']);
Route::get('/auth/facebook/redirect',  [LoginController::class, 'redirectFacebook'])->name('customer.login.facebook');
Route::get('/auth/facebook/callback',  [LoginController::class, 'authFacebook']);
Route::get('/auth/twitter/redirect',  [LoginController::class, 'redirectTwitter'])->name('customer.login.twitter');
Route::get('/auth/twitter/callback',  [LoginController::class, 'authTwitter']);


Route::get('logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);
