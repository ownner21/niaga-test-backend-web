<?php

use App\Http\Controllers\Admin\ChatController;
use App\Http\Controllers\Admin\CustomerController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Admin\OrderController;
use Illuminate\Support\Facades\Route;

Route::view('/login','admin.login')->name('admin.login');
Route::post('/login/form', [LoginController::class, 'loginForm'])->name('admin.login.form');

Route::middleware('auth:admin')->group(function () {
    Route::controller(HomeController::class)->group(function(){
        Route::get('/', 'index')->name('admin.home');
    });
    Route::controller(OrderController::class)->group(function(){
        Route::get('/orderId/{no_job}','show')->name('admin.order.show');
        Route::post('/order/data','data')->name('admin.order.data');
        Route::post('/orderId/{id}/approval','approval')->name('admin.order.approval');
        Route::post('/orderId/{id}/rejected','rejected')->name('admin.order.rejected');
        Route::post('/orderId/{id}/shipping','shipping')->name('admin.order.shipping');
        Route::post('/orderId/{id}/finished','finished')->name('admin.order.finished');
        Route::post('/orderId/{id}/return','return')->name('admin.order.return');
    });

    Route::controller(CustomerController::class)->group(function(){
        Route::get('/customer/{id}', 'profil')->name('admin.customer.profil');
    });
    
    Route::controller(ChatController::class)->group(function(){
        Route::get('chat-check/{auth}/{id}','check')->name('admin.chat.chek');
        Route::get('chat-room','room')->name('admin.chat.room');
        Route::get('chat/{key}','chat')->name('admin.chat.now');
        Route::post('chat-save','save')->name('admin.chat.save');
    });
    
});