<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ChatController extends Controller
{

    public function room()
    {
        $userRooms = DB::table('user_rooms')->where(['accountable_id' => Auth::user()->id,'accountable_type'=> 'Admin'])->get();
        return view('admin.chat-room', compact('userRooms'));
    }
    public function chat($key)
    {
        $room = DB::table('chat_rooms')->where(['key' => $key])->first();
        $userChat = DB::table('user_rooms')->where('room_id',$room->id)->get();
        $nameChat = [];
        foreach ($userChat as $user) {
            if ($user->accountable_type != 'Admin' && $user->accountable_id != Auth::user()->id) {
                if ($user->accountable_type == 'Customer') {
                    $fc = DB::table('customers')->where('id', $user->accountable_id)->select('first_name','last_name')->first();
                    $nameChat[] = $fc->first_name.' '.$fc->last_name;
                }
            }else{
                $saya = $user;
            }
        }

        $chatName = implode(' , ', $nameChat);
        $chats = DB::table('chats')->where('room_id', $room->id)->get();
        return view('admin.chat',compact('room','chatName','saya','chats'));
    }
    public function check($auth, $id)
    {   
        $key = 'Admin'.Auth::user()->id.$auth.$id;
        $find = DB::table('chat_rooms')->where(['key' => $key])->first();
        if ($find) {
            // redirect ke room id
        }else{
            $newRoom = DB::table('chat_rooms')->insertGetId(['name'=> 'room1' , 'key' => $key]);
            DB::table('user_rooms')->insert([
                ['room_id'=>$newRoom, 'accountable_id' => $id,'accountable_type'=> $auth],
                ['room_id'=>$newRoom, 'accountable_id' => Auth::user()->id,'accountable_type'=> 'Admin']
            ]);
        }
        return redirect(route('admin.chat.now',['key'=> $key]));
    }
    public function save(Request $request)
    {
        DB::table('chats')->insert(['room_id'=> $request->room_id,'user_room_id' => $request->user_room_id,'chat'=> $request->chat]);
    }
    //
}
