<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    public function profil($id)
    {
        $customer = DB::table('customers')->where('id', $id)->first();
        return view('admin.customer-profil', compact('customer'));
    }
    //
}
