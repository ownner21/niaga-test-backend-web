<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function show($no_job)
    {
        $order = Order::where('number_job', $no_job)->first();
        $logs = DB::table('order_logs')->where('order_id', $order->id)->get();
        $lastStatus = app('App\Models\OrderLog')->status($order->status);
        return view('admin.order-show', compact('order','logs','lastStatus'));
    }
    public function data(Request $request)
    {
        $perPage =10;

        if(!isset($_GET['page'])){
            $page= 1;
        }elseif($_GET['page']==""){
            $page= 1;
        }else{
            $page= $_GET['page'];
        }

        $start    = ($page - 1) * $perPage;

        $dbRaw = [];
        if ($request->has('status')) {
            if ($request->status != '') {
                $dbRaw[] = "status = '$request->status'";
            }else{
                $dbRaw[] = null;
            }
        }

        $whereRaw = implode(' AND ', $dbRaw);

        // dd($whereRaw);
        if($whereRaw != ''){
            $users = DB::table('orders')->select('id','number_job','total_qty','status')->whereRaw($whereRaw)->limit($perPage)->offset($start)->get();
        
        }else{
            $users = DB::table('orders')->select('id','number_job','total_qty','status')->limit($perPage)->offset($start)->orderBy('id', 'DESC')->get();
        }
        return response()->json(['data' => $users, 'per_page' => $perPage], 200);
    }

    public function updateStatus($id, $idStatus, $messgae = null, $adminId = null)
    {
        DB::table('orders')->where('id', $id)->update(['status' => $idStatus, 'admin_id'=> $adminId]);
        $status = app('App\Models\OrderLog')->status($idStatus, $messgae);
        DB::table('order_logs')->insert([
            'order_id' => $id,
            'logs'=> json_encode($status)
        ]);

        return $status;
    }

    public function approval($id)
    {
        $status = $this->updateStatus($id, 2, null , Auth::user()->id);
        session()->flash('sweetstatus', "'Berhasil','".$status['logs']."','success'");
        return back();
    }

    public function rejected($id, Request $request)
    {
        $messgae = $request->message;
        $status = $this->updateStatus($id, 3, $messgae, Auth::user()->id);
        session()->flash('sweetstatus', "'Berhasil','".$status['logs']."','success'");
        return back();
    }


    public function shipping($id)
    {
        $status = $this->updateStatus($id, 4);
        session()->flash('sweetstatus', "'Berhasil','".$status['logs']."','success'");
        return back();
    }
    public function finished($id)
    {
        $status = $this->updateStatus($id, 5);
        session()->flash('sweetstatus', "'Berhasil','".$status['logs']."','success'");
        return back();
    }
    public function return($id)
    {
        $status = $this->updateStatus($id, 6);
        session()->flash('sweetstatus', "'Berhasil','".$status['logs']."','success'");
        return back();
    }
}
