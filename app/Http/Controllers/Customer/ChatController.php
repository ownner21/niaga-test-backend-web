<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ChatController extends Controller
{

    public function room()
    {
        $userRooms = DB::table('user_rooms')->where(['accountable_id' => Auth::user()->id,'accountable_type'=> 'Customer'])->get();
        
        return view('customer.chat-room', compact('userRooms'));
    }
    public function chat($key)
    {
        $room = DB::table('chat_rooms')->where(['key' => $key])->first();
    
        $nameRoomSaya = app('App\Models\Chat')->nameRoomSaya($room->id, 'Customer', Auth::user()->id);
        $chatName = $nameRoomSaya['chat_name'];
        $saya = $nameRoomSaya['saya'];
        $chats = DB::table('chats')->where('room_id', $room->id)->get();
        return view('customer.chat',compact('room','chatName','saya','chats'));
    }
    public function check($auth, $id)
    {   
        $key = 'Admin'.Auth::user()->id.$auth.$id;
        $find = DB::table('chat_rooms')->where(['key' => $key])->first();
        if ($find) {
            // redirect ke room id
        }else{
            $newRoom = DB::table('chat_rooms')->insertGetId(['name'=> 'room1' , 'key' => $key]);
            DB::table('user_rooms')->insert([
                ['room_id'=>$newRoom, 'accountable_id' => $id,'accountable_type'=> $auth],
                ['room_id'=>$newRoom, 'accountable_id' => Auth::user()->id,'accountable_type'=> 'Admin']
            ]);
        }
        return redirect(route('admin.chat.now',['key'=> $key]));
    }
    public function save(Request $request)
    {
        DB::table('chats')->insert(['room_id'=> $request->room_id,'user_room_id' => $request->user_room_id,'chat'=> $request->chat]);
    }
    //
}
