<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    public function __construct()
    {
        // $this->middleware('guest:customer')->except(['logout','login']);
    }
    public function login()
    {
        return view('user.login');
    }
    public function redirectGithub(Request $request)
    {
        return Socialite::driver('github')->redirect();
    }
    public function redirectGoogle(Request $request)
    {
        return Socialite::driver('google')->redirect();
    }
    public function redirectFacebook(Request $request)
    {
        return Socialite::driver('facebook')->redirect();
    }
    public function redirectTwitter(Request $request)
    {
        return Socialite::driver('twitter')->redirect();
    }
    public function authGoogle(Request $request)
    {
        $user = Socialite::driver('google')->user();
        $where = ['email' => $user->email];
        $data = ['google_id' => $user->id, 'first_name'=> $user->name, 'photo' => $user->avatar, 'username'=>$user->id];
        return $this->loginOrRegister($where, $data);
    }
    
    public function authFacebook(Request $request)
    {
        $user = Socialite::driver('facebook')->user();
        $where = ['email' => $user->email];
        $data = ['facebook_id' => $user->id, 'first_name'=> $user->name, 'photo' => $user->avatar, 'username'=>$user->id];
        return $this->loginOrRegister($where, $data);
    }
    public function authGithub(Request $request)
    {
        $user = Socialite::driver('github')->user();
        $where = ['email' => $user->email];
        $data = ['auth->github_id' => $user->id, 'first_name'=> $user->name == null ? $user->nickname : $user->name, 'photo' => $user->avatar, 'username'=>$user->id];
        return $this->loginOrRegister($where, $data);
    }
    public function authTwitter(Request $request)
    {
        $user = Socialite::driver('twitter')->user();
        $where = ['email' => $user->email];
        $data = ['twitter_id' => $user->id, 'first_name'=> $user->name == null ? $user->nickname : $user->name, 'photo' => $user->avatar, 'username'=>$user->id];
        return $this->loginOrRegister($where, $data);
    }

    public function loginOrRegister($where, $data)
    {
        $user = Customer::updateOrCreate($where, $data);
        Auth::login($user);
        session()->flash('sweetstatus', "'Berhasil Login',' Hai ".$data['first_name']."','success'");

        if($user->number_ktp == null){
            return redirect(route('customer.profil.edit'));
        }
        return redirect(route('home'));
    }
    public function loginForm(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required|min:6',
        ]);

        $credential = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (Auth::guard('customer')->attempt($credential)){
            return redirect()->intended(route('customer.home'));
        }

        session()->flash('sweetstatus', "'Gagal','Kombinasi Email dan Password Tidak Sesuai.','error'");

        return back();
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|unique:customers,email',
            'password' => 'required|required_with:password_confirmation|min:6',
            'first_name' => 'required',
            'last_name' => 'required',
            'number_ktp' => 'required',
            'number_phone' => 'required',
            'address' => 'required',
            // 'number_npwp' => 'required'
        ]);

        $user = Customer::create($request->all());
        $user['password'] = bcrypt($request['password']);
        $user->save();
        
        Auth::guard('customer')->login($user);

        return redirect(route('customer.home'));
        
    }

    public function logout(Request $request)
    {
        Auth::guard('customer')->logout();
        return redirect('/');
    }
}
