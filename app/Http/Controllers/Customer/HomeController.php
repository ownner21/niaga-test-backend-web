<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    public function index()
    {
        return view('customer.home');
    }

    public function profiledit()
    {
        $user = DB::table('customers')->where('id', Auth::user()->id)->first();
        return view('customer.profil-edit',compact('user'));
    }

    public function profilUpdate(Request $request)
    {
        // dd($request);
        $user = DB::table('customers')->where('id',Auth::user()->id)->update($request->except('_token','_method'));
        session()->flash('sweetstatus', "'Berhasil','Berhasil Mengupdate Profil','success'");
        return back();
    }
}
