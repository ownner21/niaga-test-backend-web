<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function create()
    {
        $customer = Auth::user();
        return view('customer.order-create', compact('customer'));
    }

    public function numberJob()
    {
        return 'NG'.sprintf('%08d', Auth::user()->id).date('mdHis');
    }

    public function price(array $item)
    {
        $p = $item['p'];
        $l = $item['l'];
        $t = $item['t'];
        $weight = $item['weight'];

        $volume = $p * $l * $t;

        $priceOn  = [
            'max_start_volume' => 1000, // penentuan penggunaan tarif berat atau volume

            'price_starting_weight' => 10000, // tarif awal berdasarkan berat 
            'price_per_weight' => 7000, // tarif setelah melebihi 
            
            'starting_volume' => 10000, // tarif awal berdasarkan volume
            'price_per_volume' => 7000 // 
        ];

        $sumPrice = 0;
        if ($volume > $priceOn['max_start_volume'] ){
            $getHB = $volume % $priceOn['starting_volume'];
            if ($getHB> 0) {
                $sumPrice += $getHB;
            }else{

            }
        }else{
            $getPriceWeight = $weight % 1000;
        }
        # code...
    }
    public function store(Request $request)
    {
        $nojob = $this->numberJob();
        $items = [];
        for ($i=0; $i < count($request->item_name); $i++) { 
            $items[] = ['name' => $request->item_name[$i], 'p' => $request->item_panjang[$i], 'l' => $request->item_lebar[$i], 't' => $request->item_tinggi[$i], 'weight' => $request->item_berat[$i]];
        }
        
        $billing_address = ['name' => $request->billing_address_neme, 'phone' => $request->billing_address_phone, 'address' => $request->billing_address_address];
        $shipping_address = ['name' => $request->shipping_address_neme, 'phone' => $request->shipping_address_phone, 'address' => $request->shipping_address_address];

        $order = new Order();
        // '','admin_id', '','billing_address','shipping_address','items','total_qty','weight','volume','status'
        $order['customer_id'] = Auth::user()->id;
        $order['number_job'] = $nojob;
        $order['billing_address'] = $billing_address;
        $order['shipping_address'] = $shipping_address;
        $order['items'] = $items;
        $order['total_qty'] = count($items);
        $order->save();

        DB::table('order_logs')->insert([
            'order_id' => $order->id,
            'logs'=> json_encode(app('App\Models\OrderLog')->status(1))
        ]);

        return redirect(route('customer.order.show',['no_job' => $order->number_job]));
    }

    public function show($no_job)
    {
        $order = Order::where('number_job', $no_job)->first();
        $logs = DB::table('order_logs')->where('order_id', $order->id)->get();
        $lastStatus = app('App\Models\OrderLog')->status($order->status);
        return view('customer.order-show', compact('order','logs','lastStatus'));
    }
    public function data(Request $request)
    {
        $perPage =10;

        if(!isset($_GET['page'])){
            $page= 1;
        }elseif($_GET['page']==""){
            $page= 1;
        }else{
            $page= $_GET['page'];
        }

        $start    = ($page - 1) * $perPage;

        $users = DB::table('orders')->where('customer_id',Auth::user()->id)->limit($perPage)->offset($start)->orderBy('id', 'DESC')->select('id','number_job','total_qty','status')->get();
        return response()->json(['data' => $users, 'per_page' => $perPage], 200);
    }
}
