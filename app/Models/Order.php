<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'customer_id','admin_id', 'number_job','billing_address','shipping_address','items','total_qty','status'
    ];
    protected $casts = [
        'items' => 'array',
        'billing_address' => 'array',
        'shipping_address' => 'array'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
