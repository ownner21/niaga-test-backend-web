<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRoom extends Model
{
    protected $fillable = [
        'name','accountable_id','accountable_type','room_id'
    ];

    public function user()
    {
        if ($this->accountable_type == 'Admin') {
            return $this->belongsTo(Admin::class, 'accountable_id');
        }else{
            return $this->belongsTo(Customer::class, 'accountable_id');
        }
    }
}
