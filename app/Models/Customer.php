<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    protected $fillable = [
        'email','auth','password','address','first_name','last_name','number_ktp','number_phone','number_npwp','photo'
    ];

    protected $casts = [
        'auth'
    ];
}
