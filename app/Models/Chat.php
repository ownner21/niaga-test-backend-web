<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Chat extends Model
{
    protected $fillable = ['room_id','chat','user_room_id','created_at'];

    public function userChat()
    {
        return $this->belongsTo(UserRoom::class,'user_room_id');
    }

    public function siapa($user_room_id)
    {
        $user = DB::table('user_rooms')->where('id', $user_room_id)->select('accountable_id','accountable_type')->first();
        if ($user->accountable_type == 'Customer') {
            $fc = DB::table('customers')->where('id', $user->accountable_id)->select('first_name','last_name')->first();
            $nameChat = $fc->first_name.' '.$fc->last_name;
        }else{
          $fc = DB::table('admins')->where('id', $user->accountable_id)->select('name')->first();
            $nameChat = $fc->name;
        }
        return $nameChat;
    }

    public function nameRoom($roomId, $auth, $id)
    {
        $userChat = DB::table('user_rooms')->where('room_id',$roomId)->get();
        $nameChat = [];
        foreach ($userChat as $user) {
            if ($user->accountable_type != $auth && $user->accountable_id != $id) {
                if ($user->accountable_type == 'Customer') {
                    $fc = DB::table('customers')->where('id', $user->accountable_id)->select('first_name','last_name')->first();
                    $nameChat[] = $fc->first_name.' '.$fc->last_name;
                }else{
                    $fc = DB::table('admins')->where('id', $user->accountable_id)->select('name')->first();
                    $nameChat[] = $fc->name;
                }
            }else{
                $saya = $user;
            }
        }

        $chatName = implode(' , ', $nameChat);
        return $chatName;
    }
    public function nameRoomSaya($roomId, $auth, $id)
    {
        $userChat = DB::table('user_rooms')->where('room_id',$roomId)->get();
        $nameChat = [];
        foreach ($userChat as $user) {
            if ($user->accountable_type != $auth && $user->accountable_id != $id) {
                if ($user->accountable_type == 'Customer') {
                    $fc = DB::table('customers')->where('id', $user->accountable_id)->select('first_name','last_name')->first();
                    $nameChat[] = $fc->first_name.' '.$fc->last_name;
                }else{
                    $fc = DB::table('admins')->where('id', $user->accountable_id)->select('name')->first();
                    $nameChat[] = $fc->name;
                }
            }else{
                $saya = $user;
            }
        }

        $chatName = implode(' , ', $nameChat);
        return ['chat_name' => $chatName, 'saya'=> $saya];
    }
}
