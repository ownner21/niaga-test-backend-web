<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderLog extends Model
{
    protected $fillable = [
        'order_id','logs'
    ];

    public function status($id, $messgae = null)
    {
        $data = [
            1 => [
                'status' => 'Submited',
                'logs'=> 'Orderan berhasil dibuat oleh customer, menunggu dikonfirmasi admin'
            ],
            2 => [
                'status' => 'Approval',
                'logs'=> 'Admin telah mengkonfirmasi job orderan, Kurir menyiapkan penjemputan paket'
            ],
            3 => [
                'status' => 'Rejected',
                'logs'=> 'Admin menolak orderan dengan alasan: '.$messgae
            ],
            4 => [
                'status' => 'Shipping',
                'logs'=> 'Kurir telah menerima paket dan akan diteruskan ke lokasi tujuan'
            ],
            5 => [
                'status' => 'Finished',
                'logs'=> 'Ordera berhasil diselesaikan'
            ],
            6 => [
                'status' => 'Return',
                'logs'=> 'Alamat orderan Tidak Sesuai, paket akan dikembalikan pada lokasi penjemputan'
            ],
        ];
        return $data[$id];
    }
    public function getStatus($id)
    {
        $data = [
            1 => 'Submited',
            2 => 'Approval',
            3 => 'Rejected',
            4 => 'Shipping',
            5 => 'Finished',
            6 => 'Return'
        ];
        return $data[$id];
    }
}
