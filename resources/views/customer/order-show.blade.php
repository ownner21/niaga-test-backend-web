@extends('customer.template')

@section('content')
<div class="row">
  <div class="col-sm-8">
    <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Order</li>
      </ol>
    </nav>
    Number Job 
    <h5>{{$order->number_job}}</h5>
    Status : {{$lastStatus['status']}} 
    <hr><br>
    <table class="table table-sm">
      <tr>
        <th colspan="2">User Order</th>
      </tr>
        <tr><td>Nama</td><td>: {{$order->customer->first_name . ' '. $order->customer->last_name}}</a> </td></tr>
        <tr><td>Nomor Hp</td><td>: {{$order->customer->number_phone}}</td></tr>
        <tr><td>Address</td><td>: {{$order->customer->address}}</td></tr>
      <tr>
      <tr>
        <th colspan="2"><br>Alamat Pickup</th>
      </tr>
        <tr><td>Nama</td><td>: {{$order->billing_address['name']}}</td></tr>
        <tr><td>Nomor Hp</td><td>: {{$order->billing_address['phone']}}</td></tr>
        <tr><td>Address</td><td>: {{$order->billing_address['address']}}</td></tr>
      <tr>
        <th colspan="2"><br>Alamat Tujuan</th>
      </tr>
        <tr><td>Nama</td><td>: {{$order->shipping_address['name']}}</td></tr>
        <tr><td>NoAddressmor Hp</td><td>: {{$order->shipping_address['phone']}}</td></tr>
        <tr><td>Address</td><td>: {{$order->shipping_address['address']}}</td></tr>
    </table>

    <table class="table table-sm mt-5">
      <tr class="text-center">
        <th>Nama Barang</th>
        <th>Berat</th>
        <th>Panjang</th>
        <th>Lebar</th>
        <th>Tinggi</th>
      </tr>
      @foreach ($order->items as $item)
          <tr class="text-center">
            <td class="text-start">{{$item['name']}}</td>
            <td>{{$item['weight']}}</td>
            <td>{{$item['p']}}</td>
            <td>{{$item['l']}}</td>
            <td>{{$item['t']}}</td>
          </tr>
      @endforeach
    </table>
  </div>


  <div class="col-md-4">
    <!-- Section: Timeline -->
<section class="">
  

  <ul class="timeline">
    @foreach ($logs as $log)
    <?php $ld = json_decode($log->logs,true); ?>
    <li class="timeline-item mb-5">
      <h5 class="fw-bold">{{$ld['status']}}</h5>
      <p class="text-muted mb-2 fw-bold">{{hari_tanggal($log->created_at)}}</p>
      <p class="text-muted">{{$ld['logs']}}</p>
    </li>
    @endforeach
  </ul>
</section>
<!-- Section: Timeline -->
  </div>

</div>
@endsection

{{-- 'customer_id','admin_id', 'number_job','billing_address','shipping_address','items','total_qty','status' --}}
