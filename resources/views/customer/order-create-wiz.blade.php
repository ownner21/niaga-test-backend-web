@extends('customer.template')
@section('css')
<link rel="stylesheet" href="{{asset('vendors/wizzard/style.css')}}">
@endsection
@section('content')
<!-- partial:index.partial.html -->
<form method="POST" action="">
  <div class="row">
  <div class="col-md-8">
    <div id="app">
        <step-navigation :steps="steps" :currentstep="currentstep">
        </step-navigation>
        
        <div v-show="currentstep == 1">
            <h1>Pengirim</h1>
              <div class="row">
                <div class="col-12">
                  <p class="mt-4">Deskripsikan Pengirim, secara default pengirim adalah adalah pemilik akun</p>
                </div>
              </div>
              <div class="row">
                <div class="col col-12">
                  <div class="form-floating mb-3">
                    <input type="text" name="billing_address_neme" required value="{{$customer->first_name. ' '. $customer->last_name}} " class="form-control {{$errors->has('billing_address_neme') ? 'is-invalid' : ''}}" {{$errors->has('last_name') ? 'onchange="isValid("billing_address_neme","error-billing_address_neme")"' : ''}} id="billing_address_neme" placeholder="Nama Pengirim">
                    <label for="billing_address_neme">Nama Pengirim</label>
                    @if ($errors->has('billing_address_neme'))
                      <p class="error mt-2 text-danger" id="error-billing_address_neme" for="billing_address_neme">
                        {{ $errors->first('billing_address_neme') }}
                      </p>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col col-12">
                  <div class="form-floating mb-3">
                    <input type="number" name="billing_address_phone" required value="{{$customer->number_phone}}" class="form-control {{$errors->has('billing_address_phone') ? 'is-invalid' : ''}}" {{$errors->has('last_name') ? 'onchange="isValid("billing_address_phone","error-billing_address_phone")"' : ''}} id="billing_address_phone" placeholder="Nomor HP Pengirim">
                    <label for="billing_address_phone">Nomor HP Pengirim</label>
                    @if ($errors->has('billing_address_phone'))
                      <p class="error mt-2 text-danger" id="error-billing_address_phone" for="billing_address_phone">
                        {{ $errors->first('billing_address_phone') }}
                      </p>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col col-12">
                  <div class="form-floating mb-3">
                    <input type="number" name="billing_address_address" required value="{{$customer->address}}" class="form-control {{$errors->has('billing_address_address') ? 'is-invalid' : ''}}" {{$errors->has('last_name') ? 'onchange="isValid("billing_address_address","error-billing_address_address")"' : ''}} id="billing_address_address" placeholder="Alamat Tujuan">
                    <label for="billing_address_address">Alamat Pickup</label>
                    @if ($errors->has('billing_address_address'))
                      <p class="error mt-2 text-danger" id="error-billing_address_address" for="billing_address_address">
                        {{ $errors->first('billing_address_address') }}
                      </p>
                    @endif
                  </div>
                </div>
              </div>
            
        </div>

        <div v-show="currentstep == 2">
            <h1>Lokasi Tujuan</h1>
              <div class="row">
                <div class="col-12">
                  <p class="mt-4">Deskripsikan Pengirim, secara default pengirim adalah adalah pemilik akun</p>
                </div>
              </div>
              <div class="row">
                <div class="col col-12">
                  <div class="form-floating mb-3">
                    <input type="text" name="shipping_address_neme" required value="" class="form-control {{$errors->has('shipping_address_neme') ? 'is-invalid' : ''}}" {{$errors->has('last_name') ? 'onchange="isValid("shipping_address_neme","error-shipping_address_neme")"' : ''}} id="shipping_address_neme" placeholder="Nama Penerima">
                    <label for="shipping_address_neme">Nama Penerima</label>
                    @if ($errors->has('shipping_address_neme'))
                      <p class="error mt-2 text-danger" id="error-shipping_address_neme" for="shipping_address_neme">
                        {{ $errors->first('shipping_address_neme') }}
                      </p>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col col-12">
                  <div class="form-floating mb-3">
                    <input type="number" name="shipping_address_phone" required value="" class="form-control {{$errors->has('shipping_address_phone') ? 'is-invalid' : ''}}" {{$errors->has('last_name') ? 'onchange="isValid("shipping_address_phone","error-shipping_address_phone")"' : ''}} id="shipping_address_phone" placeholder="Nomor HP Penerima">
                    <label for="shipping_address_phone">Nomor HP Penerima</label>
                    @if ($errors->has('shipping_address_phone'))
                      <p class="error mt-2 text-danger" id="error-shipping_address_phone" for="shipping_address_phone">
                        {{ $errors->first('shipping_address_phone') }}
                      </p>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col col-12">
                  <div class="form-floating mb-3">
                    <input type="number" name="shipping_address_address" required value="{{$customer->address}}" class="form-control {{$errors->has('shipping_address_address') ? 'is-invalid' : ''}}" {{$errors->has('last_name') ? 'onchange="isValid("shipping_address_address","error-shipping_address_address")"' : ''}} id="shipping_address_address" placeholder="Alamat Pickup">
                    <label for="shipping_address_address">Alamat Tujuan</label>
                    @if ($errors->has('shipping_address_address'))
                      <p class="error mt-2 text-danger" id="error-shipping_address_address" for="shipping_address_address">
                        {{ $errors->first('shipping_address_address') }}
                      </p>
                    @endif
                  </div>
                </div>
              </div>
        </div>

        <div v-show="currentstep == 3">
            <h1>Item</h1>
            <p>Product siap kirim</p>
            <div class="form-group">
                <label for="textarea">Example textarea</label>
                <textarea class="form-control" name="textarea" rows="4"></textarea>
            </div>
            <div class="form-group">
                <label for="file">File input</label>
                <input type="file" class="form-control-file" name="file" aria-describedby="fileHelp">
                <small id="fileHelp" class="form-text text-muted">This is some placeholder block-level help text for the above input. It's a bit lighter and easily wraps to a new line.</small>
            </div>
        </div>
        <div v-show="currentstep == 4">
          <h1>Step 4</h1>
          <div class="form-group">
              <label for="textarea">Example textarea</label>
              <textarea class="form-control" name="textarea" rows="4"></textarea>
          </div>
          <div class="form-group">
              <label for="file">File input</label>
              <input type="file" class="form-control-file" name="file" aria-describedby="fileHelp">
              <small id="fileHelp" class="form-text text-muted">This is some placeholder block-level help text for the above input. It's a bit lighter and easily wraps to a new line.</small>
          </div>
      </div>

        <step v-for="step in steps" :currentstep="currentstep" :key="step.id" :step="step" :stepcount="steps.length" @step-change="stepChanged">
        </step>


        <script type="x-template" id="step-template">
            <div class="step-wrapper" :class="stepWrapperClass">
                <button type="button" class="btn btn-primary" @click="lastStep" :disabled="firststep">
                    Back
                </button>
                <button type="button" class="btn btn-primary" @click="nextStep" :disabled="laststep">
                    Next
                </button>
                <button type="submit" class="btn btn-primary" v-if="laststep">
                    Submit
                </button>
            </div>
        </script>
    </div>
</div>
</div>
</form>

@endsection
@section('script')
  <script src='https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.4/vue.js'></script>
  <script  src="{{asset('vendors/wizzard/script.js')}}"></script>
@endsection

