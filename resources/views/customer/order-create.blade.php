@extends('customer.template')

@section('content')
  
<form action="{{route('customer.order.store')}}" method="post"> @csrf
    <div id="hiddenInput"></div>
<div class="row">
  <div class="col-md-8"></div>
  <div class="col-md-8">
    <div class="row">
      <div class="col-12">
        <h2>New Order</h2>
        <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">New Order</li>
            </ol>
          </nav>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus quos nihil quisquam aut sapiente perferendis atque molestiae totam vitae doloribus, provident accusamus? Dolorem adipisci laboriosam, vitae mollitia ut voluptates saepe!</p>
      </div>
    </div>
<hr>
    <div class="row">
        <div class="row">
        <div class="col-12">
            <b>Pengirim</b>
            <p>Deskripsikan Pengirim, secara default pengirim adalah adalah pemilik akun</p>
        </div>
        </div>
        <div class="row">
        <div class="col col-12">
            <div class="form-floating mb-3">
            <input type="text" name="billing_address_neme" required value="{{$customer->first_name. ' '. $customer->last_name}} " class="form-control {{$errors->has('billing_address_neme') ? 'is-invalid' : ''}}" {{$errors->has('last_name') ? 'onchange="isValid("billing_address_neme","error-billing_address_neme")"' : ''}} id="billing_address_neme" placeholder="Nama Pengirim">
            <label for="billing_address_neme">Nama Pengirim</label>
            @if ($errors->has('billing_address_neme'))
                <p class="error mt-2 text-danger" id="error-billing_address_neme" for="billing_address_neme">
                {{ $errors->first('billing_address_neme') }}
                </p>
            @endif
            </div>
        </div>
        </div>
        <div class="row">
        <div class="col col-12">
            <div class="form-floating mb-3">
            <input type="number" name="billing_address_phone" required value="{{$customer->number_phone}}" class="form-control {{$errors->has('billing_address_phone') ? 'is-invalid' : ''}}" {{$errors->has('last_name') ? 'onchange="isValid("billing_address_phone","error-billing_address_phone")"' : ''}} id="billing_address_phone" placeholder="Nomor HP Pengirim">
            <label for="billing_address_phone">Nomor HP Pengirim</label>
            @if ($errors->has('billing_address_phone'))
                <p class="error mt-2 text-danger" id="error-billing_address_phone" for="billing_address_phone">
                {{ $errors->first('billing_address_phone') }}
                </p>
            @endif
            </div>
        </div>
        </div>
        <div class="row">
        <div class="col col-12">
            <div class="form-floating mb-3">
            <input type="text" name="billing_address_address" required value="{{$customer->address}}" class="form-control {{$errors->has('billing_address_address') ? 'is-invalid' : ''}}" {{$errors->has('last_name') ? 'onchange="isValid("billing_address_address","error-billing_address_address")"' : ''}} id="billing_address_address" placeholder="Alamat Tujuan">
            <label for="billing_address_address">Alamat Pickup</label>
            @if ($errors->has('billing_address_address'))
                <p class="error mt-2 text-danger" id="error-billing_address_address" for="billing_address_address">
                {{ $errors->first('billing_address_address') }}
                </p>
            @endif
            </div>
        </div>
        </div>
    </div>
<hr>

    <div class="row">
        <div class="row">
            <div class="col-12">
                <b>Lokasi Tujuan</b>
              <p>Lokasi barang/item akan diturunkan</p>
            </div>
          </div>
          <div class="row">
            <div class="col col-12">
              <div class="form-floating mb-3">
                <input type="text" name="shipping_address_neme" required value="{{env('APP_ENV') != 'production' ? 'nama penerima' : ''}}" class="form-control {{$errors->has('shipping_address_neme') ? 'is-invalid' : ''}}" {{$errors->has('last_name') ? 'onchange="isValid("shipping_address_neme","error-shipping_address_neme")"' : ''}} id="shipping_address_neme" placeholder="Nama Penerima">
                <label for="shipping_address_neme">Nama Penerima</label>
                @if ($errors->has('shipping_address_neme'))
                  <p class="error mt-2 text-danger" id="error-shipping_address_neme" for="shipping_address_neme">
                    {{ $errors->first('shipping_address_neme') }}
                  </p>
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col col-12">
              <div class="form-floating mb-3">
                <input type="number" name="shipping_address_phone" required value="{{env('APP_ENV') != 'production' ? '0812131212' : ''}}" class="form-control {{$errors->has('shipping_address_phone') ? 'is-invalid' : ''}}" {{$errors->has('last_name') ? 'onchange="isValid("shipping_address_phone","error-shipping_address_phone")"' : ''}} id="shipping_address_phone" placeholder="Nomor HP Penerima">
                <label for="shipping_address_phone">Nomor HP Penerima</label>
                @if ($errors->has('shipping_address_phone'))
                  <p class="error mt-2 text-danger" id="error-shipping_address_phone" for="shipping_address_phone">
                    {{ $errors->first('shipping_address_phone') }}
                  </p>
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col col-12">
              <div class="form-floating mb-3">
                <input type="text" name="shipping_address_address" required value="{{env('APP_ENV') != 'production' ? 'alamat penerima' : ''}}" class="form-control {{$errors->has('shipping_address_address') ? 'is-invalid' : ''}}" {{$errors->has('last_name') ? 'onchange="isValid("shipping_address_address","error-shipping_address_address")"' : ''}} id="shipping_address_address" placeholder="Alamat Pickup">
                <label for="shipping_address_address">Alamat Tujuan</label>
                @if ($errors->has('shipping_address_address'))
                  <p class="error mt-2 text-danger" id="error-shipping_address_address" for="shipping_address_address">
                    {{ $errors->first('shipping_address_address') }}
                  </p>
                @endif
              </div>
            </div>
          </div>
    </div>
<hr>
    <div class="row">
        <div class="row">
            <div class="col-12">
                <button type="button" class="btn btn-primary float-end" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                    Tambah
                  </button>
                  
                <b>Product / Item</b>
              <p>Lokasi barang/item akan diturunkan</p>

              <ul class="list-group list-group-flush"  id="rowItem">
              </ul>
              
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
          <div class="d-grid gap-2">
            <button class="btn btn-outline-primary btn-lg" type="submit">Order</button>
          </div>
        </div>
      </div>
    
  </div>
</div>
</form>


<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header p-5 pb-4 border-bottom-0">
            <h2 class="fw-bold mb-0">Tambah Item</h2>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
        <div class="modal-body p-5 pt-0">
            <div class="form-floating mb-3">
                <input type="text" name="item_name" class="form-control" value="{{env('APP_ENV') != 'production' ? 'benda' : ''}}" id="item_name" onchange="isValid('item_name')" placeholder="Alamat Pickup">
                <label for="item_name">Nama Item</label>
            </div>
            <div class="form-floating mb-3">
                <input type="number" name="item_panjang" class="form-control" value="{{env('APP_ENV') != 'production' ? '1' : ''}}" id="item_panjang" onchange="isValid('item_panjang')"  placeholder="Panjang Item">
                <label for="item_panjang">Panjang Item (cm)</label>
            </div>
            <div class="form-floating mb-3">
                <input type="number" name="item_lebar" class="form-control" value="{{env('APP_ENV') != 'production' ? '2' : ''}}" id="item_lebar" onchange="isValid('item_lebar')"  placeholder="Lebar Item">
                <label for="item_lebar">Lebar Item (cm)</label>
            </div>
            <div class="form-floating mb-3">
                <input type="number" name="item_tinggi" class="form-control" value="{{env('APP_ENV') != 'production' ? '2' : ''}}" id="item_tinggi" onchange="isValid('item_tinggi')"  placeholder="Tinggi">
                <label for="item_tinggi">Tinggi Item (cm)</label>
            </div>
            <div class="form-floating mb-3">
                <input type="number" name="item_berat" class="form-control" value="{{env('APP_ENV') != 'production' ? '1000' : ''}}" id="item_berat" onchange="isValid('item_berat')"  placeholder="Berat">
                <label for="item_berat">Berat (gram)</label>
            </div>
        </div>
        <br>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="addItem()">Tambah Item</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
    
<script>
    function isValid(idInput, idText = null) {
        document.getElementById(idInput).classList.remove("is-invalid");
        if (idText != null) {
            document.getElementById(idText).innerHTML = '';
        }
    }

    function isInValid(idInput, idText, valueError = null) {
        document.getElementById(idInput).classList.add("is-invalid");
        if (idText != null) {
            document.getElementById(idText).innerHTML = valueError;
        }
    }
    function addHInputItem(item, val) {
        console.log(item)

        var inputHidden = '<input type="hidden" name="'+item+'[]" value="'+val+'">'
        $("#hiddenInput").append(inputHidden)

    }

    function addItem() {
        var nameItem = document.getElementById('item_name').value;
        var panjangItem = document.getElementById('item_panjang').value;
        var lebarItem = document.getElementById('item_lebar').value;
        var tinggiItem = document.getElementById('item_tinggi').value;
        var beratItem = document.getElementById('item_berat').value;

        var error = 0;
        if (nameItem == '') { isInValid('item_name',null); error++ } else { isValid('item_name',null);}
        if (panjangItem == '') { isInValid('item_panjang',null); error++ }
        if (lebarItem == '') { isInValid('item_lebar',null); error++ }
        if (tinggiItem == '') { isInValid('item_tinggi',null); error++ }
        if (beratItem == '') { isInValid('item_berat',null); error++ }

        if (error == 0) {
            $('#staticBackdrop').modal('hide'); 
            $('body').removeClass('modal-open'); 
            $('.modal-backdrop').remove();

            var microtime = (Date.now() % 1000) / 1000;

            addHInputItem('item_name',nameItem)
            addHInputItem('item_panjang',panjangItem)
            addHInputItem('item_lebar',lebarItem)
            addHInputItem('item_tinggi',tinggiItem)
            addHInputItem('item_berat',beratItem)

            var addRow = " <li class='list-group-item' id='item"+microtime+"'> <button type='button' onclick='removeItem("+microtime+")' class='btn-close float-end'></button><b>"+nameItem+"</b> Berat "+beratItem+" gram <br> <small> ( Panjang "+panjangItem +" * Lebar "+ lebarItem +" * Tinggi"+ tinggiItem +") </small></li>"
            $("#rowItem").append(addRow)

            removeValue('item_name');
            removeValue('item_panjang');
            removeValue('item_lebar');
            removeValue('item_tinggi');
            removeValue('item_berat');


        }

        // console.log(nameItem);
    }

    function removeValue(cl) {
        var nameItem = document.getElementById(cl).value = "";
    }


    function removeItem(id){
        document.getElementById('item'+id).remove();
    }


</script>
@endsection