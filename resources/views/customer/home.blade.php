@extends('customer.template')
@section('content')
    

@if (Auth::check())
  <h1>Hai <span class="text-danger">{{Auth::user()->first_name . ' '. Auth::user()->last_name}}</span></h1>
  <p class="fs-5 col-md-8 mt-3">Anda Dapat Melakukan Pengajuan Pengiriman. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vitae eveniet nemo harum blanditiis aperiam. Officia ipsam doloremque soluta, veritatis ex, explicabo ea dolore tenetur numquam nostrum reiciendis hic! Dolor, hic.</p>
      <a href="{{route('customer.order.create')}}" class="btn btn-primary px-4">New Order</a>
      <a href="{{route('customer.profil.edit')}}" class="btn btn-outline-primary px-4">Edit Profil</a>
      <a href="{{route('customer.chat.room')}}" class="btn btn-outline-primary px-4">Chat Room</a>
  <hr class="col-3 col-md-2 mb-5">

  <table class="table table-sm">
    <tr>
      <td>Number Job</td>
      <td>Total Qty</td>
      <td>Status</td>
      <td></td>
    </tr>
    <tbody id="dataUser"></tbody>
  </table>
  
@else
  <h1>Get started with msyaifudin</h1>
  <p class="fs-5 col-md-8">Quickly and easily get started with Bootstrap's compiled, production-ready files with this barebones example featuring some basic HTML and helpful links. Download all our examples to get started.</p>
<div class="mb-5">
    <a href="{{route('customer.register')}}" class="btn btn-outline-primary">Daftar</a>
  @include('customer.modal-login')
</div>
@endif
@endsection


@if (Auth::check())
@section('script')
<script>
    var currenPage
    var maxPage = false
    var cPage = parseInt(location.search.split('page=')[1]);
    currenPage = !cPage ? 1 : cPage;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    reloadData()

    function setURLSearchParam(key, value) {
        const url = new URL(window.location.href);
        url.searchParams.set(key, value);
        window.history.pushState({ path: url.href }, '', url.href);
    }


    function btnNext() {
        var page = currenPage+1;
        setURLSearchParam('page',page)
        document.getElementById('btnPrev').classList.remove("disabled");
        if (maxPage) {
            document.getElementById('btnNext').classList.add("disabled");
        }else{
            reloadData();
            currenPage = page;
        }
    }
    
    function btnPrev() {
        var page = currenPage-1;
        setURLSearchParam('page',page)
        document.getElementById('btnNext').classList.remove("disabled");
        if(page == 0){
            document.getElementById('btnPrev').classList.add("disabled");
        }else{
            maxPage = false
            document.getElementById('btnPrev').classList.remove("disabled");
            reloadData();
            currenPage = page;
        }
    }

    function reloadData() {
        $.ajax({ 
            type: 'POST', 
            url: "{{route('customer.order.data')}}"+window.location.search, 
            dataType: 'json',
            success: function (data) { 
                $('#dataUser').empty()
                if (data.data.length < data.per_page) {
                    maxPage = true
                    // document.getElementById('btnNext').classList.remove("disabled");
                    // console.log('dibawah')
                }

                var status = ['Submited','Approval','Rejected', 'Shipping','Finished','Return'];

                $.each(data.data, function(index, element) {
                    var birt = new Date(element.date_of_birt);
                    var dDate = birt.getDate() % 2 == 1 ? 'Ganjil' : 'Genap';

                    var wDate = element.week % 2 == 1 ? 'Ganjil' : 'Genap';
                    
                    var st = element.status-1
                    $('#dataUser').append('<tr><td>'+element.number_job+'</td><td>'+element.total_qty+'</td><td>'+status[st]+'</td><td><a href="/customer/orderId/'+element.number_job+'" class="btn btn-sm btn-outline-secondary">Detail</a></td><</tr>');

                });
            }
        });
    }

    </script>
    
@endsection
@endif