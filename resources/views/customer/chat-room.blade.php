@extends('customer.template')
@section('css')

@endsection

@section('content')
<div class="row">
  <div class="col-sm-8">
    <h4>Chat Room</h4>
    <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Chat Room</li>
      </ol>
    </nav>


    <div class="list-group">
        @foreach ($userRooms as $userRoom)
        <?php 
          $room = DB::table('chat_rooms')->where(['id' => $userRoom->room_id])->first();
          $chatName = app('App\Models\Chat')->nameRoom($room->id, 'Customer',Auth::user()->id);

        ?>
        <a href="{{route('customer.chat.now',['key'=> $room->key])}}" class="list-group-item list-group-item-action" aria-current="true">{{$chatName}}</a>
        @endforeach
    </div>

</div>

  

</div>
@endsection
