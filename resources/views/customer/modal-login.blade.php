<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
    Login Customer
    </button>
    
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header p-5 pb-4 border-bottom-0">
            <!-- <h5 class="modal-title">Modal title</h5> -->
            <h2 class="fw-bold mb-0">Login</h2>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-5 pt-0">
                <form action="{{route('customer.login.form')}}" method="POST"> @csrf
                    <div class="form-floating mb-3">
                    <input type="email" name="email" value="{{env('APP_ENV') != 'production' ? 'email@email.com' : ''}}" class="form-control rounded-3" id="floatingInput" placeholder="name@example.com">
                    <label for="floatingInput">Email address</label>
                    </div>
                    <div class="form-floating mb-3">
                    <input type="password" name="password" value="{{env('APP_ENV') != 'production' ? 'sandi1234' : ''}}" class="form-control rounded-3" id="floatingPassword" placeholder="Password">
                    <label for="floatingPassword">Password</label>
                    </div>
                    <button class="w-100 mb-2 btn btn-lg rounded-3 btn-primary" type="submit">Login</button>
                    <small class="text-muted">By clicking Sign up, you agree to the terms of use.</small>
                </form>
                    <hr class="my-4">
                    <h2 class="fs-5 fw-bold mb-3">Or use a third-party</h2>
                    {{-- <button class="w-100 py-2 mb-2 btn btn-outline-dark rounded-3" type="submit">
                    <svg class="bi me-1" width="16" height="16"><use xlink:href="#twitter"/></svg>
                    Sign up with Twitter
                    </button>
                    <button class="w-100 py-2 mb-2 btn btn-outline-primary rounded-3" type="submit">
                    <svg class="bi me-1" width="16" height="16"><use xlink:href="#facebook"/></svg>
                    Sign up with Facebook
                    </button> --}}
                    <a href="{{route('customer.login.github')}}" class="w-100 py-2 mb-2 btn btn-outline-secondary rounded-3">
                    <svg class="bi me-1" width="16" height="16"><use xlink:href="#github"/></svg>
                    SignIn Or SignUp with GitHub
                    </a>
        </div>
        
        </div>
    </div>
</div>