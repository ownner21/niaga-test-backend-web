@extends('customer.template')

@section('content')
  
<form action="{{route('customer.register.store')}}" method="post"> @csrf
<div class="row">
  <div class="col-md-8"></div>
  <div class="col-md-8">
    <div class="row">
      <div class="col-12">
        <h2>Daftar</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus quos nihil quisquam aut sapiente perferendis atque molestiae totam vitae doloribus, provident accusamus? Dolorem adipisci laboriosam, vitae mollitia ut voluptates saepe!</p>
      </div>
    </div>
    <div class="row">
      <div class="col col-6"><div class="form-floating mb-3">
          <input type="text" name="first_name" required value="{{env('APP_ENV') != 'production' ? 'Depan' : ''}}" class="form-control {{$errors->has('first_name') ? 'is-invalid' : ''}}" {{$errors->has('last_name') ? 'onchange="isValid("first_name","error-first_name")"' : ''}} id="first_name" placeholder="Nama Depan">
          <label for="first_name">Nama Depan</label>
          @if ($errors->has('first_name'))
            <p class="error mt-2 text-danger" id="error-first_name" for="first_name">
              {{ $errors->first('first_name') }}
            </p>
          @endif
        </div>
      </div>
      <div class="col col-6">
        <div class="form-floating mb-3">
          <input type="text" name="last_name" required value="{{env('APP_ENV') != 'production' ? 'Belakang' : ''}}" class="form-control {{$errors->has('last_name') ? 'is-invalid' : ''}}" {{$errors->has('last_name') ? 'onchange="isValid("last_name","error-last_name")"' : ''}} id="last_name" placeholder="Nama Belakang">
          <label for="last_name">Nama Belakang</label>
          @if ($errors->has('last_name'))
            <p class="error mt-2 text-danger" id="error-last_name" for="last_name">
              {{ $errors->first('last_name') }}
            </p>
          @endif
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col col-12"><div class="form-floating mb-3">
          <input type="text" name="address" required value="{{env('APP_ENV') != 'production' ? 'Alamat User' : ''}}" class="form-control {{$errors->has('address') ? 'is-invalid' : ''}}" {{$errors->has('last_name') ? 'onchange="isValid("address","error-address")"' : ''}} id="address" placeholder="Alamat">
          <label for="address">Alamat</label>
          @if ($errors->has('address'))
            <p class="error mt-2 text-danger" id="error-address" for="address">
              {{ $errors->first('address') }}
            </p>
          @endif
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col col-12"><div class="form-floating mb-3">
          <input type="number" name="number_ktp" required value="{{env('APP_ENV') != 'production' ? '34232323980900' : ''}}" class="form-control {{$errors->has('number_ktp') ? 'is-invalid' : ''}}" {{$errors->has('last_name') ? 'onchange="isValid("number_ktp","error-number_ktp")"' : ''}} id="number_ktp" placeholder="Nomor KTP/NIK">
          <label for="number_ktp">Nomor KTP/NIK</label>
          @if ($errors->has('number_ktp'))
            <p class="error mt-2 text-danger" id="error-number_ktp" for="number_ktp">
              {{ $errors->first('number_ktp') }}
            </p>
          @endif
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col col-12">
        <div class="form-floating mb-3">
          <input type="number" name="number_phone" required value="{{env('APP_ENV') != 'production' ? '0897129192912' : ''}}" class="form-control {{$errors->has('number_phone') ? 'is-invalid' : ''}}" {{$errors->has('last_name') ? 'onchange="isValid("number_phone","error-number_phone")"' : ''}} id="number_phone" placeholder="Nomor HP">
          <label for="number_phone">Nomor HP</label>
          @if ($errors->has('number_phone'))
            <p class="error mt-2 text-danger" id="error-number_phone" for="number_phone">
              {{ $errors->first('number_phone') }}
            </p>
          @endif
        </div>
      </div>

    </div>
    <div class="row">
      <div class="col col-12">
        <div class="form-floating mb-3">
          <input type="number" name="number_npwp" value="{{env('APP_ENV') != 'production' ? '192192912' : ''}}" class="form-control {{$errors->has('number_npwp') ? 'is-invalid' : ''}}" {{$errors->has('last_name') ? 'onchange="isValid("number_npwp","error-number_npwp")"' : ''}} id="number_npwp" placeholder="Nomor NPWP">
          <label for="number_npwp">Nomor NPWP</label>
          @if ($errors->has('number_npwp'))
            <p class="error mt-2 text-danger" id="error-npwp" for="npwp">
              {{ $errors->first('npwp') }}
            </p>
          @endif
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="form-floating mb-3">
          <input type="email" name="email" required value="{{env('APP_ENV') != 'production' ? 'email@email.com' : ''}}" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" {{$errors->has('last_name') ? 'onchange="isValid("email","error-email")"' : ''}} id="email" placeholder="email@email.com">
          <label for="email">Email address</label>
          @if ($errors->has('email'))
            <p class="error mt-2 text-danger" id="error-email" for="email">
              {{ $errors->first('email') }}
            </p>
          @endif
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="form-floating mb-3">
          <input type="password" name="password" required value="{{env('APP_ENV') != 'production' ? 'sandi1234' : ''}}" class="form-control {{$errors->has('password') ? 'is-invalid' : ''}}" {{$errors->has('last_name') ? 'onchange="isValid("password","error-password")"' : ''}} id="password" placeholder="Password">
          <label for="password">Password</label>
          @if ($errors->has('password'))
            <p class="error mt-2 text-danger" id="error-password" for="password">
              {{ $errors->first('password') }}
            </p>
          @endif
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="form-floating mb-3">
          <input type="password" name="password_confirmation" required value="{{env('APP_ENV') != 'production' ? 'sandi1234' : ''}}" class="form-control" id="cPassword" placeholder="Confirmasi Password">
          <label for="cPassword">Confirmasi Password</label>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="form-check mb-3">
          <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" {{env('APP_ENV') != 'production' ? 'checked' : ''}} required>
          <label class="form-check-label" for="flexCheckDefault">
            <a href="#" class="text-muted text-decoration-none">Saya Menyetujui Persyaratan</a>
          </label>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="d-grid gap-2">
          <button class="btn btn-outline-primary btn-lg" type="submit">Daftar</button>
        </div>
      </div>
    </div>
  </div>
</div>
</form>

@endsection

@section('script')
    
<script>
    function isValid(idInput, idText) {
        document.getElementById(idInput).classList.remove("is-invalid");
        document.getElementById(idText).innerHTML = '';
    }
</script>
@endsection