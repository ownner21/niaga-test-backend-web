@extends('admin.template')

@section('css')
@endsection
@section('content')
      <div class="card">
        <div class="py-2 px-4 border-bottom d-none d-lg-block">
          <div class="d-flex align-items-center py-1">
            <div class="position-relative mr-4">
              <img src="https://bootdey.com/img/Content/avatar/avatar3.png" class="rounded-circle mr-1" alt="Sharon Lessman" width="40" height="40">
            </div>
            <div class="flex-grow-1 p-2">
              <strong>{{$chatName}}</strong>
              <div class="text-muted small"><em>-</em></div>
            </div>
            <div>
            </div>
          </div>
        </div>

        <div class="position-relative">
          <div class="chat-messages p-4">
            @foreach ($chats as $chat)
            <?php 
              $nameChat = app('App\Models\Chat')->siapa($chat->user_room_id);
            ?>
            @if ($chat->user_room_id == $saya->id)
              <div class="chat-message-right mb-4">
                <div class="p-2">
                  <img src="https://bootdey.com/img/Content/avatar/avatar1.png" class="rounded-circle mr-1" alt="Chris Wood" width="40" height="40">
                  <div class="text-muted small text-nowrap mt-2">
                    {{ date('H:i', strtotime($chat->created_at))}}
                  </div>
                </div>
                <div class="flex-shrink-1 bg-light rounded py-2 px-3 mr-3">
                  <div class="font-weight-bold mb-1">You</div>{{$chat->chat}}
                </div>
              </div>
            @else
              <div class="chat-message-left pb-4">
                <div class="p-2">
                  <img src="https://bootdey.com/img/Content/avatar/avatar3.png" class="rounded-circle mr-1" alt="Sharon Lessman" width="40" height="40">
                  <div class="text-muted small text-nowrap mt-2">
                    {{ date('H:i', strtotime($chat->created_at))}}
                  </div>
                </div>
                <div class="flex-shrink-1 bg-light rounded py-2 px-3 ml-3">
                  <div class="font-weight-bold mb-1">{{$nameChat}}</div>
                  {{$chat->chat}}
                </div>
              </div>
            @endif

            @endforeach
          </div>
        </div>

        <div class="flex-grow-0 py-3 px-4 border-top">
          <div class="input-group">
            {{-- <div class="chat-input bg-primary" id="chatInput" contenteditable="">

            </div> --}}
            <input type="text" class="form-control" id="chatInput" placeholder="Type your message">
            <button class="btn btn-primary">Send</button>
          </div>
        </div>
      </div>
@endsection


@section('script')
    
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script src="https://cdn.socket.io/4.0.1/socket.io.min.js" integrity="sha384-LzhRnpGmQP+lOvWruF/lgkcqD+WDVt9fU3H4BWmwP5u5LTmkUGafMcpZKNObVMLU" crossorigin="anonymous"></script>


<script>
    var name = '{{Auth::user()->name}}'
    var roomId = '{{$room->id}}'
    var userRoomId = '{{$saya->id}}'


    $(function() {
        let ip_address = '{{env("NODE_ADDRESS","127.0.0.1")}}';
        let socket_port = '{{env("NODE_PORT","1234")}}';
        let socket = io(ip_address + ':' + socket_port);

        let chatInput = $('#chatInput');

        chatInput.keypress(function(e) {
            let message = $(this).val();

            if(e.which === 13 && !e.shiftKey) {
                var obj = {message: message, user: userRoomId, name: name, roomId : roomId};
                socket.emit('sendChatToServer', obj);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({ 
                    type: 'POST', 
                    url: "{{route('admin.chat.save')}}"+window.location.search, 
                    dataType: 'json',
                    data: {
                      room_id : roomId, 
                      user_room_id : userRoomId,
                      chat: message
                    },
                });
                $(this).val("")
                return false;
            }
        });


        socket.on('sendChatToClient', (message) => {
          if (roomId == message.roomId) {
            var today = new Date();
            var hourMinute = today.getHours() + ":" + today.getMinutes();
            
              if (userRoomId == message.user) {
                  var divPengirimSaya = '<div class="chat-message-right mb-4"><div class="p-2"><img src="https://bootdey.com/img/Content/avatar/avatar1.png" class="rounded-circle mr-1" alt="Chris Wood" width="40" height="40"><div class="text-muted small text-nowrap mt-2">'+hourMinute+'</div></div><div class="flex-shrink-1 bg-light rounded py-2 px-3 mr-3"><div class="font-weight-bold mb-1">You</div>'+message.message+'</div></div>'
                  $('.chat-messages').append(divPengirimSaya);
              }else{
                  var divPengirim = '<div class="chat-message-left pb-4"><div class="p-2"><img src="https://bootdey.com/img/Content/avatar/avatar3.png" class="rounded-circle mr-1" alt="Sharon Lessman" width="40" height="40"><div class="text-muted small text-nowrap mt-2">'+hourMinute+'</div></div><div class="flex-shrink-1 bg-light rounded py-2 px-3 ml-3"><div class="font-weight-bold mb-1">'+message.name+'</div>'+message.message+'</div></div>'
                  $('.chat-messages').append(divPengirim);
              }
          }
        });
    });
</script>

