@extends('admin.template')
@section('content')
<div class="row justify-content-md-center">
    <div class="col-sm-6">
        <h4>Login Admin</h4>
        <form action="{{route('admin.login.form')}}" method="POST"> @csrf
            <div class="form-floating mb-3">
            <input type="email" name="email" value="{{env('APP_ENV') != 'production' ? 'admin@email.com' : ''}}" class="form-control rounded-3" id="floatingInput" placeholder="name@example.com">
            <label for="floatingInput">Email address</label>
            </div>
            <div class="form-floating mb-3">
            <input type="password" name="password" value="{{env('APP_ENV') != 'production' ? 'rahasia' : ''}}" class="form-control rounded-3" id="floatingPassword" placeholder="Password">
            <label for="floatingPassword">Password</label>
            </div>
            <button class="w-100 mb-2 btn btn-lg rounded-3 btn-primary" type="submit">Login</button>
            <small class="text-muted">By clicking Sign up, you agree to the terms of use.</small>
        </form>
    </div>
</div>
@endsection