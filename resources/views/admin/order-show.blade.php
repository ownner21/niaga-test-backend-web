@extends('admin.template')
@section('css')

@endsection

@section('content')
<div class="row">
  <div class="col-sm-8">
    <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Order</li>
      </ol>
    </nav>
    Number Job 
    <h5>{{$order->number_job}}</h5>
    Status : {{$lastStatus['status']}} 
    <hr><br>
    
    <table class="table table-sm">
      <tr>
        <th colspan="2">User Order</th>
      </tr>
        <tr><td>Nama</td><td>: <a href="{{route('admin.customer.profil',['id'=>$order->customer_id])}}" class="btn btn-link">{{$order->customer->first_name . ' '. $order->customer->last_name}}</a> </td></tr>
        <tr><td>Nomor Hp</td><td>: {{$order->customer->number_phone}}</td></tr>
        <tr><td>Address</td><td>: {{$order->customer->address}}</td></tr>
      <tr>
      <tr>
        <th colspan="2"><br>Alamat Pickup</th>
      </tr>
        <tr><td>Nama</td><td>: {{$order->billing_address['name']}}</td></tr>
        <tr><td>Nomor Hp</td><td>: {{$order->billing_address['phone']}}</td></tr>
        <tr><td>Address</td><td>: {{$order->billing_address['address']}}</td></tr>
      <tr>
        <th colspan="2"><br>Alamat Tujuan</th>
      </tr>
        <tr><td>Nama</td><td>: {{$order->shipping_address['name']}}</td></tr>
        <tr><td>NoAddressmor Hp</td><td>: {{$order->shipping_address['phone']}}</td></tr>
        <tr><td>Address</td><td>: {{$order->shipping_address['address']}}</td></tr>
    </table>

    <table class="table table-sm mt-5">
      <tr class="text-center">
        <th>Nama Barang</th>
        <th>Berat</th>
        <th>Panjang</th>
        <th>Lebar</th>
        <th>Tinggi</th>
      </tr>
      @foreach ($order->items as $item)
          <tr class="text-center">
            <td class="text-start">{{$item['name']}}</td>
            <td>{{$item['weight']}}</td>
            <td>{{$item['p']}}</td>
            <td>{{$item['l']}}</td>
            <td>{{$item['t']}}</td>
          </tr>
      @endforeach
    </table>
  </div>

  <div class="col-md-4">
    <!-- Section: Timeline -->
<section class="">
  

  <ul class="timeline">
    @foreach ($logs as $log)
    <?php $ld = json_decode($log->logs,true); ?>
    <li class="timeline-item mb-5">
      <h5 class="fw-bold">{{$ld['status']}}</h5>
      <p class="text-muted mb-2 fw-bold">{{hari_tanggal($log->created_at)}}</p>
      <p class="text-muted">{{$ld['logs']}}</p>
    </li>
    @endforeach

    @if ($order->status == 1)

    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item" role="presentation">
        <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home-tab-pane" type="button" role="tab" aria-controls="home-tab-pane" aria-selected="true">Konfirmasi</button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile-tab-pane" type="button" role="tab" aria-controls="profile-tab-pane" aria-selected="false">Tolak</button>
      </li>
    </ul>
    <br>
    <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade show active" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab" tabindex="0">
        <p>Jika orderan ini layak untuk dilanjutkan ke status selanjutnya klik tombol konfirmasi</p>
        <form action="{{route('admin.order.approval',['id'=>$order->id])}}" id="upprove-form" method="post"> @csrf
        </form>

          <button class="btn btn-success" onclick="sendNotif()">Konfirmasi</button>
      </div>
      <div class="tab-pane fade" id="profile-tab-pane" role="tabpanel" aria-labelledby="profile-tab" tabindex="0">
        <form action="{{route('admin.order.rejected',['id'=>$order->id])}}" method="post"> @csrf
          <div class="form-floating mb-3">
            <textarea class="form-control" name="message" required placeholder="Kirim pesan alasan Penolakan" id="floatingTextarea2" style="height: 100px"></textarea>
            <label for="floatingTextarea2">Alasan Penolakan</label>
          </div>
          <button class="btn btn-outline-danger" type="submit">Rejected</button>
        </form>
      </div>
    </div>
      
    @elseif ($order->status == 2)
    <form action="{{route('admin.order.shipping',['id'=>$order->id])}}" method="post"> @csrf
      <button class="btn btn-success" type="submit"> Pengiriman</button>
    </form>
    @elseif ($order->status == 4)
    <form action="{{route('admin.order.finished',['id'=>$order->id])}}" method="post"> @csrf
      <button class="btn btn-success" type="submit"> Konfirmasi Selesai</button>
    </form>

    <form action="{{route('admin.order.return',['id'=>$order->id])}}" method="post"> @csrf
      <button class="btn btn-outline-danger" type="submit"> Return</button>
    </form>
    @endif
  </ul>
</section>
<!-- Section: Timeline -->
  </div>

</div>
@endsection

@section('script')
@if ($order->status == 1)
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script src="https://cdn.socket.io/4.0.1/socket.io.min.js" integrity="sha384-LzhRnpGmQP+lOvWruF/lgkcqD+WDVt9fU3H4BWmwP5u5LTmkUGafMcpZKNObVMLU" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/push.js/1.0.8/push.min.js"></script>

    <script>

    let ip_address = '{{env("NODE_ADDRESS","127.0.0.1")}}';
    let socket_port = '{{env("NODE_PORT","1234")}}';
    let socket = io(ip_address + ':' + socket_port);

      function sendNotif() {
        var userId = "customer{{$order->customer_id}}"
        var noJob = "{{$order->number_job}}"
        var message = "Telah Diuprove Admin"

        var obj = {userId: userId, noJob: noJob, message : message};
        socket.emit('cekNotification', obj);
        document.getElementById('upprove-form').submit();
      }
    </script>
@endif
@endsection

{{-- 'customer_id','admin_id', 'number_job','billing_address','shipping_address','items','total_qty','status' --}}
