@extends('admin.template')
@section('content')
    

  <h1>Hai <span class="text-danger">{{Auth::user()->name}}</span></h1><hr>
    <a href="{{route('admin.chat.room')}}" class="btn btn-outline-primary">Chat</a>
    <hr>
  <table class="table table-sm">
    <tr>
      <th>Number Job</th>
      <th>Total Qty</th>
      <th>Status</th>
      <td></td>
    </tr>
    <tbody id="dataUser"></tbody>
  </table>
  
@endsection


@if (Auth::check())
@section('script')
<script>
    var currenPage
    var maxPage = false
    var cPage = parseInt(location.search.split('page=')[1]);
    currenPage = !cPage ? 1 : cPage;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    reloadData()

    function setURLSearchParam(key, value) {
        const url = new URL(window.location.href);
        url.searchParams.set(key, value);
        window.history.pushState({ path: url.href }, '', url.href);
    }


    function btnNext() {
        var page = currenPage+1;
        setURLSearchParam('page',page)
        document.getElementById('btnPrev').classList.remove("disabled");
        if (maxPage) {
            document.getElementById('btnNext').classList.add("disabled");
        }else{
            reloadData();
            currenPage = page;
        }
    }
    
    function btnPrev() {
        var page = currenPage-1;
        setURLSearchParam('page',page)
        document.getElementById('btnNext').classList.remove("disabled");
        if(page == 0){
            document.getElementById('btnPrev').classList.add("disabled");
        }else{
            maxPage = false
            document.getElementById('btnPrev').classList.remove("disabled");
            reloadData();
            currenPage = page;
        }
    }

    function reloadData() {
        $.ajax({ 
            type: 'POST', 
            url: "{{route('admin.order.data')}}"+window.location.search, 
            dataType: 'json',
            success: function (data) { 
                $('#dataUser').empty()
                if (data.data.length < data.per_page) {
                    maxPage = true
                    // document.getElementById('btnNext').classList.remove("disabled");
                    // console.log('dibawah')
                }
                var status = ['Submited','Approval','Rejected', 'Shipping','Finished','Return'];
                $.each(data.data, function(index, element) {
                    var birt = new Date(element.date_of_birt);
                    var dDate = birt.getDate() % 2 == 1 ? 'Ganjil' : 'Genap';

                    var wDate = element.week % 2 == 1 ? 'Ganjil' : 'Genap';
                    
                    var st = element.status-1
                    $('#dataUser').append('<tr><td>'+element.number_job+'</td><td>'+element.total_qty+'</td><td>'+status[st]+'</td><td><a href="/admin/orderId/'+element.number_job+'" class="btn btn-sm btn-outline-secondary">Detail</a></td><</tr>');
                });
            }
        });
    }

    </script>
    
@endsection
@endif