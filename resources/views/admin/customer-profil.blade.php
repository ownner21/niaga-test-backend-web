@extends('admin.template')

@section('content')
  
<div class="row">
  <div class="col-md-8"></div>
  <div class="col-md-8">
    <div class="row">
      <div class="col-12">
        <h2>Profil <span class="text-danger">{{$customer->first_name}}</span></h2>
        <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Profil Customer</li>
            </ol>
          </nav>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus quos nihil quisquam aut sapiente perferendis atque molestiae totam vitae doloribus, provident accusamus? Dolorem adipisci laboriosam, vitae mollitia ut voluptates saepe!</p>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <table class="table">
          <tr><td>Nama Depan</td><th>{{$customer->first_name}}</th></tr>
          <tr><td>Nama Belakang</td><th>{{$customer->last_name}}</th></tr>
          <tr><td>Alamat</td><th>{{$customer->address}}</th></tr>
          <tr><td>Nomor KTP/NIK</td><th>{{$customer->number_ktp}}</th></tr>
          <tr><td>Nomor HP</td><th>{{$customer->number_phone}}</th></tr>
          <tr><td>Nomor NPWP</td><th>{{$customer->number_npwp}}</th></tr>
        </table>

        <a href="{{route('admin.chat.chek',['auth'=> 'Customer', 'id' => $customer->id])}}" class="btn btn-danger">Chat Customer</a>
      </div>
    </div>
  </div>
</div>

@endsection

@section('script')
    
@endsection